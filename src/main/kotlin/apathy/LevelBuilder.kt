package apathy

import apathy.entities.Entity
import apathy.entities.Exit
import apathy.entities.Wall
import java.io.BufferedInputStream
import java.io.FileInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

/**
 * Each game level class has to be a subclass of the LevelBuilder, which manages
 * building the level from .level files and additional level-specific code
 */
abstract class LevelBuilder {

    protected abstract val name: String
    protected open val time = 30
    private val markers = mutableMapOf<Char, Pos>()
    private val levelTicks = mutableListOf<() -> Unit>()
    private var level: Level? = null

    /**
     * Returns the size bounds of a level based on the .level file
     */
    private fun readLevelSize(name: String): Pos {
        val lines = Files.readAllLines(Paths.get("Levels/$name.level"), StandardCharsets.UTF_8)
        return Pos(lines.maxBy { it.length }!!.length, lines.size)
    }

    /**
     * Reads a level from a .level file and loads marked locations
     */
    private fun readLevel(name: String): Level {
        val level = Level(time * 1000L)
        val centeringOffset = (Pos(Level.WIDTH, Level.HEIGHT) - readLevelSize(name)) / 2
        val scanner = Scanner(BufferedInputStream(FileInputStream("Levels/$name.level")), StandardCharsets.UTF_8)
        var y = 0
        var textWritingMode = false
        while (scanner.hasNextLine()) {
            val line = scanner.nextLine()
            line.forEachIndexed { x, char ->
                val pos = Pos(x, y) + centeringOffset
                when {
                    char == '(' -> textWritingMode = true
                    char == ')' -> textWritingMode = false
                    textWritingMode -> level.place(pos, Wall(Sprite(char)))
                    char.isLetterOrDigit() -> {
                        if (markers.containsKey(char)) {
                            System.err.println("In file: $name.level, duplicate marker: $char")
                        }
                        markers += char to pos
                    }
                    char == '☻' -> level.place(pos, level.player)
                    char == '▦' -> level.place(pos, Exit())
                    char == '✈' -> level.place(pos, Exit(true))
                    char != ' ' -> level.place(pos, Wall(Sprite(char)))
                }
            }
            y++
        }
        return level
    }

    /**
     * Places an Entity in a location marked by the marker char
     * in the corresponding .level file
     */
    protected fun place(marker: Char, vararg entities: Entity) {
        entities.forEach {
            level!!.place(markers[marker]!!, it)
        }
        markers.remove(marker)
    }

    /**
     * Adds a lambda to be executed on each level tick in this level
     */
    protected fun addLevelTick(tick: () -> Unit) {
        levelTicks += tick
    }

    /**
     * For level-specific initialization code
     */
    abstract fun initialize()

    /**
     * Builds and returns the level, can be called multiple times to obtain
     * multiple copies of the level (for example for restarting the level to a clean state)
     */
    fun build(): Level {
        markers.clear()
        levelTicks.clear()
        return readLevel(name).also {
            this.level = it
            initialize()
            if (markers.isNotEmpty()) {
                System.err.println("In file: $name.level, unused markers: ${markers.toList()}")
            }
            it.levelTick = {
                levelTicks.forEach { it.invoke() }
            }
        }
    }

}
