package apathy
import apathy.entities.Entity

/**
 * The player's inventory, which can contain a number of items
 * and maintains a selection cursor
 */
class Inventory {

    private val items = mutableListOf<Entity>()
    private var selectionIndex = -1

    fun add(item: Entity) {
        items += item
        selectionIndex = items.indexOf(item)
    }

    private fun remove(item: Entity) {
        val index = items.indexOf(item)
        items -= item
        if (selectionIndex >= index) {
            moveSelectionDown()
        }
        if (items.isEmpty()) {
            selectionIndex = -1
        }
    }

    fun removeSelected(): Entity? {
        return if (selectionIndex > -1) {
            val item = items[selectionIndex]
            remove(item)
            item
        } else null
    }

    fun moveSelectionDown() {
        if (items.isNotEmpty()) {
            selectionIndex++
            if (selectionIndex > items.lastIndex) {
                selectionIndex = items.lastIndex
            }
        }
    }

    fun moveSelectionUp() {
        if (items.isNotEmpty()) {
            selectionIndex--
            if (selectionIndex < 0) {
                selectionIndex = 0
            }
        }
    }

    fun getNames(): List<String> {
        return items.map { it.name }
    }

    fun getSelectionIndex(): Int {
        return selectionIndex
    }

    fun clear() {
        while (items.isNotEmpty()) {
            removeSelected()
        }
    }

}
