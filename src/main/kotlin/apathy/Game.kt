package apathy

import apathy.levels.*

/**
 * Contains the state of the game
 */
class Game {

    private val levels = listOf(LevelOne(), LevelTwo(), LevelThree(), LevelFour(), LevelFive(), LevelSix(), LevelSeven(), LevelEight(), LevelNine(), LevelTen())
    private var levelIndex = 0
    var level = levels[levelIndex].build()
    val inventory = Inventory()
    var isPlayable = true
    private set
    var gameOverReason: String? = null
    var gameCompleted = false
    private set
    var levelWon = false
    private set
    var stepsTaken = 0
    private set
    var isPaused = false
    private set
    private var deathTotal = 0
    var stepsTotal = 0
    private set

    /**
     * Tick the level if the game is running
     */
    fun resolveTicks(deltaTime: Long) {
        if (!isPaused) {
            level.tick(deltaTime)
        }
    }

    /**
     * Get the current two status lines
     */
    fun getStatus(): Pair<String, String> {
        val firstLine = mutableListOf<String>()
        val playerStack = level.getTile(level.player.pos!!)!!.getEntities()
        val playerIndex = playerStack.indexOfLast { it is Player }
        val entityIndex = playerStack.indexOfLast { it !is Player }
        if (entityIndex != -1) {
            val entity = playerStack[entityIndex]
            firstLine += if (entityIndex < playerIndex) {
                "Standing on: ${entity.name}."
            } else {
                "Covered under: ${entity.name}."
            }
            if (entity.pickupable) {
                firstLine += "Press Space to pickup."
            }
        }

        val secondLine = mutableListOf<String>()
        secondLine += "Time left: ${getFormattedTimeLeft()}."
        secondLine += "Steps taken: $stepsTaken."
        secondLine += "Deaths: $deathTotal."

        return firstLine.joinToString(" ") to secondLine.joinToString(" ")
    }

    private fun getFormattedTimeLeft(): String {
        val seconds = level.timeLeft.toDouble() / 1000L
        return when {
            seconds >= 10 -> String.format("%.0fs", seconds)
            seconds >= 0 -> String.format("%.1fs", seconds)
            else -> "None"
        }
    }

    /**
     * Move the player
     */
    fun move(direction: Direction) {
        if (!isPlayable || isPaused) return
        val pos = level.player.pos!!.to(direction)
        if (!level.isBlocking(pos)) {
            level.place(pos, level.player)
            stepsTaken++
        }
    }

    fun pickup() {
        if (!isPlayable) return
        val pickupable = level.getTile(level.player.pos)!!.getEntities().lastOrNull { it.pickupable }
        if (pickupable != null) {
            level.remove(pickupable)
            inventory.add(pickupable)
            pickupable.onPickup()
        }
    }

    fun drop() {
        if (!isPlayable) return
        inventory.removeSelected()?.let {
            level.place(level.player.pos!!, it)
            level.player.resurface()
        }
    }

    fun loseGame(reason: String) {
        isPlayable = false
        gameOverReason = reason
        deathTotal++
    }

    fun winGame() {
        isPlayable = false
        levelWon = true
    }

    /**
     * Continues with the game:
     * If level was lost - restarts the level
     * If level was won - starts the next level
     * If the last level was won - wins the game
     */
    fun continuePlaying() {
        if (!isPlayable) {
            if (gameCompleted) {
                gameCompleted = false
                levelIndex = 0
                deathTotal = 0
                stepsTotal = 0
                startLevel()
            } else {
                if (levelWon) {
                    levelIndex++
                    if (levelIndex > levels.lastIndex) {
                        gameCompleted = true
                    } else {
                        startLevel()
                    }
                } else {
                    startLevel()
                }
            }
        }
    }

    private fun startLevel() {
        level = levels[levelIndex].build()
        inventory.clear()
        stepsTotal += stepsTaken
        stepsTaken = 0
        isPlayable = true
        levelWon = false
    }

    fun togglePause() {
        isPaused = !isPaused
    }

    fun restartLevel() {
        level.player.kill("You gave up on this life")
    }

}
