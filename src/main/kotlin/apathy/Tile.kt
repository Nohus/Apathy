package apathy

import apathy.entities.Entity

/**
 * A single tile on the game level
 */
class Tile {

    private val stack: MutableList<Entity> = mutableListOf()

    /**
     * Place an entity on the tile
     */
    fun place(entity: Entity) {
        stack += entity
    }

    /**
     * Remove an entity from the tile
     */
    fun remove(entity: Entity) {
        stack -= entity
    }

    /**
     * Get the Drawable that should represent this tile at this moment
     */
    fun getDrawable(): Drawable {
        return if (stack.isNotEmpty()) {
            stack.last().getDrawable()
        } else {
            Drawable(/*'⋅'*/' ')
        }
    }

    fun getEntities(): List<Entity> {
        return stack.toList()
    }

}
