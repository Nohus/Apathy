package apathy

/**
 * Calculates time deltas between frames and current FPS
 */
class FrameTimer {

    private var frameTimes = mutableListOf<Long>()
    private var lastFrameTime = System.currentTimeMillis()
    var deltaTime = 0L
    private set

    /**
     * Call on each frame
     */
    fun recordFrame() {
        val frameTime = System.currentTimeMillis()
        deltaTime = frameTime - lastFrameTime
        lastFrameTime = frameTime

        frameTimes.removeAll { it < frameTime - 1000 }
        frameTimes.add(frameTime)
    }

    fun getFPS(): Int {
        return frameTimes.size
    }

}
