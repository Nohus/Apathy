package apathy
import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.Terminal
import com.googlecode.lanterna.terminal.swing.AWTTerminalFontConfiguration
import com.googlecode.lanterna.terminal.swing.SwingTerminalFontConfiguration
import java.awt.Font
import java.awt.Window
import java.io.File

const val GAME_WINDOW_WIDTH = 60
const val GAME_WINDOW_HEIGHT = 20
const val SIDEBAR_WINDOW_WIDTH = 20
const val INVENTORY_WINDOW_HEIGHT = 9
const val STATUS_WINDOW_HEIGHT = 4

val terminal = createSwingTerminal()
val game = Game()

fun main(args: Array<String>) {
    terminal.enterPrivateMode()
    terminal.setCursorVisible(false)
    gameLoop()
    terminal.exitPrivateMode()
    terminal.close()
}

fun createSwingTerminal(): Terminal {
    val swingTerminal = DefaultTerminalFactory()
            .setTerminalEmulatorTitle("Apathy – by Marcin Wisniowski for G54GAM")
            .setInitialTerminalSize(TerminalSize(GAME_WINDOW_WIDTH + SIDEBAR_WINDOW_WIDTH, GAME_WINDOW_HEIGHT + STATUS_WINDOW_HEIGHT))
            .setTerminalEmulatorFontConfiguration(SwingTerminalFontConfiguration(
                    true,
                    AWTTerminalFontConfiguration.BoldMode.EVERYTHING_BUT_SYMBOLS,
                    Font.createFont(Font.TRUETYPE_FONT, File("DejaVuSansMono.ttf")).deriveFont(28f)))
            .createSwingTerminal()
    swingTerminal.isVisible = true
    return swingTerminal
}

/**
 * The main game loop of Apathy
 */
fun gameLoop() {
    val drawingManager = DrawingManager()
    val inputManager = InputManager()
    val frameTimer = FrameTimer()
    val window = terminal as Window
    while (window.isShowing) {
        frameTimer.recordFrame()
        inputManager.processInput()
        game.resolveTicks(frameTimer.deltaTime)
        drawingManager.drawGame()
    }
}
