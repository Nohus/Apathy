package apathy

import apathy.Direction.DOWN

/**
 * Executes draw calls to draw the game on the screen
 */
class DrawingManager {

    private fun draw(pos: Pos, char: Char) {
        terminal.setCursorPosition(pos.x, pos.y)
        terminal.putCharacter(char)
    }

    private fun draw(pos: Pos, drawable: Drawable) {
        terminal.setCursorPosition(pos.x, pos.y)
        terminal.setForegroundColor(drawable.color)
        terminal.putCharacter(drawable.character)
        terminal.resetColorAndSGR()
    }

    private fun draw(pos: Pos, text: String) {
        terminal.setCursorPosition(pos.x, pos.y)
        text.forEach {
            terminal.putCharacter(it)
        }
    }

    private fun drawWindow(start: Pos, width: Int, height: Int) {
        val end = start + Pos(width - 1, height - 1)
        // Vertical lines
        for (x in start.x + 1 until end.x) {
            draw(Pos(x, start.y), '━')
            draw(Pos(x, end.y), '━')
        }
        // Horizontal lines
        for (y in start.y + 1 until end.y) {
            draw(Pos(start.x, y), '┃')
            draw(Pos(end.x, y), '┃')
        }
        // Corners
        draw(start, '┏')
        draw(Pos(end.x, start.y), '┓')
        draw(Pos(start.x, end.y), '┗')
        draw(end, '┛')
        // Clear inside
        for (x in start.x + 1 until end.x) {
            for (y in start.y + 1 until end.y) {
                draw(Pos(x, y), ' ')
            }
        }
    }

    private fun drawMap(start: Pos, level: Level) {
        for (x in 0 until Level.WIDTH) {
            for (y in 0 until Level.HEIGHT) {
                draw(start + Pos(x, y), level.getDrawable(Pos(x, y)))
            }
        }
    }

    private fun drawStatus(start: Pos) {
        val (first, second) = game.getStatus()
        draw(start, first)
        draw(start.to(DOWN), second)
    }

    private fun drawInventory(start: Pos, inventory: Inventory) {
        draw(start + Pos(4, 0), "Your items")
        if (inventory.getNames().isEmpty()) {
            draw(start + Pos(7, 1), "None")
        } else {
            var line = 0
            inventory.getNames().forEach {
                val chevron = if (line == inventory.getSelectionIndex()) "[X]" else "[ ]"
                draw(start + Pos(0, 1 + line), "$chevron ${it.capitalize()}")
                line++
            }
        }
    }

    private fun drawInfo(start: Pos) {
        draw(start + Pos(5, 0), "Controls")
        draw(start + Pos(0, 1), "Move: WASD")
        draw(start + Pos(0, 2), "Pickup: Space")
        draw(start + Pos(0, 3), "Items: Up/Down")
        draw(start + Pos(0, 4), "Drop/use: Enter")
        draw(start + Pos(0, 5), "Pause: P")
        draw(start + Pos(0, 6), "Restart: R")
    }

    private fun drawMessageWindow(center: Pos, messages: List<String>) {
        val width = messages.maxBy { it.length }!!.length + 2
        val pos = center + Pos(-width / 2, -1 - messages.size / 2)
        drawWindow(pos, width, messages.size + 2)
        var line = 1
        messages.forEach {
            val offset = (width - it.length) / 2
            draw(pos + Pos(offset, line), it)
            line++
        }
    }

    private fun drawGameStoppedMessage() {
        if (game.isPaused) {
            drawMessageWindow(Pos(GAME_WINDOW_WIDTH / 2, GAME_WINDOW_HEIGHT - 5), listOf("PAUSED", "Press P to play"))
        } else if (!game.isPlayable) {
            val text = when {
                game.gameCompleted -> listOf("YOU WON", "Congratulations!", "Taken a total of ${game.stepsTotal} steps", "Press Enter to restart the game")
                game.levelWon -> listOf("LEVEL COMPLETED", "Press Enter to continue")
                else -> listOf("YOU ARE DEAD", game.gameOverReason!!, "Press Enter to restart")
            }
            drawMessageWindow(Pos(GAME_WINDOW_WIDTH / 2, GAME_WINDOW_HEIGHT - 5), text)
        }
    }

    fun drawGame() {
        drawWindow(Pos(0, 0), GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT) // Game
        drawWindow(Pos(GAME_WINDOW_WIDTH, 0), SIDEBAR_WINDOW_WIDTH, INVENTORY_WINDOW_HEIGHT) // Inventory
        drawWindow(Pos(GAME_WINDOW_WIDTH, INVENTORY_WINDOW_HEIGHT), SIDEBAR_WINDOW_WIDTH, GAME_WINDOW_HEIGHT - INVENTORY_WINDOW_HEIGHT) // Instructions
        drawWindow(Pos(0, GAME_WINDOW_HEIGHT), GAME_WINDOW_WIDTH + SIDEBAR_WINDOW_WIDTH, STATUS_WINDOW_HEIGHT) // Status
        drawMap(Pos(1, 1), game.level)
        drawStatus(Pos(1, GAME_WINDOW_HEIGHT + 1))
        drawInventory(Pos(GAME_WINDOW_WIDTH + 1, 1), game.inventory)
        drawInfo(Pos(GAME_WINDOW_WIDTH + 1, INVENTORY_WINDOW_HEIGHT + 1))
        drawGameStoppedMessage()
        terminal.flush()
    }

}
