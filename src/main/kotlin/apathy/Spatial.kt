package apathy

import apathy.Direction.*
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL

/**
 * This file contains simple classes and functions related to
 * positions, directions and orientations
 */

enum class Orientation {
    VERTICAL, HORIZONTAL
}

enum class Direction {
    LEFT, RIGHT, UP, DOWN
}

fun Direction.reverse(): Direction {
    return when (this) {
        LEFT -> RIGHT
        RIGHT -> LEFT
        UP -> DOWN
        DOWN -> UP
    }
}

fun Direction.orientation(): Orientation {
    return when (this) {
        LEFT -> HORIZONTAL
        RIGHT -> HORIZONTAL
        UP -> VERTICAL
        DOWN -> VERTICAL
    }
}

fun Direction.rotate(clockwise: Boolean): Direction {
    val rotated = when (this) {
        LEFT -> UP
        UP -> RIGHT
        RIGHT -> DOWN
        DOWN -> LEFT
    }
    return if (clockwise) {
        rotated
    } else {
        rotated.reverse()
    }
}

data class Pos(
        val x: Int,
        val y: Int
) {

    override fun toString(): String {
        return "$x, $y"
    }

}

fun Pos.to(direction: Direction): Pos {
    return when (direction) {
        LEFT -> Pos(x - 1, y)
        RIGHT -> Pos(x + 1, y)
        UP -> Pos(x, y - 1)
        DOWN -> Pos(x, y + 1)
    }
}

operator fun Pos.plus(other: Pos): Pos {
    return Pos(x + other.x, y + other.y)
}

operator fun Pos.minus(other: Pos): Pos {
    return Pos(x - other.x, y - other.y)
}

operator fun Pos.div(other: Int): Pos {
    return Pos(x / other, y / other)
}
