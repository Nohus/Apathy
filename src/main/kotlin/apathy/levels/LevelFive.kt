package apathy.levels

import apathy.Direction.LEFT
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.GREEN

class LevelFive : LevelBuilder() {

    override val name = "5"

    override fun initialize() {
        val switch1 = FloorSwitch()
        val switch2 = FloorSwitch()
        place('G', switch1)
        place('H', switch2)
        place('A', Door(VERTICAL, { switch1.pressed }))
        place('B', Door(VERTICAL, { switch1.pressed || switch2.pressed }))
        place('C', Door(VERTICAL, { switch2.pressed }))
        place('F', Door(VERTICAL, { switch1.pressed }))
        place('E', Door(VERTICAL, { switch1.pressed || switch2.pressed }))
        place('D', Door(VERTICAL, { switch2.pressed }))

        place('I', FlyingTriangle(LEFT))
        val key = Key("exit", GREEN)
        place('J', key)
        val lock = KeyLock(key)
        place('K', lock)
        place('L', Door(HORIZONTAL, { lock.unlocked }))
    }

}
