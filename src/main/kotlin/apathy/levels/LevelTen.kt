package apathy.levels

import apathy.Direction
import apathy.Direction.LEFT
import apathy.Direction.RIGHT
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.RED

class LevelTen : LevelBuilder() {

    override val name = "10"
    override val time = 120

    override fun initialize() {
        // Main laser
        val laserKey = Key("laser", RED)
        val laserLock = KeyLock(laserKey)
        place('A', laserLock)
        place('B', LaserGenerator())
        place('C', Door(VERTICAL, { laserLock.unlocked }))
        place('D', Door(VERTICAL, { laserLock.unlocked }))
        place('E', Door(HORIZONTAL, { laserLock.unlocked }))
        // Left laser
        val leftLaserTarget = LaserTarget()
        place('F', leftLaserTarget)
        place('G', LaserGenerator())
        place('H', Door(VERTICAL, { leftLaserTarget.active }))
        place('I', Door(VERTICAL, { leftLaserTarget.active }))
        place('J', Door(HORIZONTAL, { leftLaserTarget.active }))
        // Right laser
        val rightLaserTarget = LaserTarget()
        place('K', rightLaserTarget)
        place('L', LaserGenerator())
        place('M', Door(VERTICAL, { rightLaserTarget.active }))
        place('N', Door(VERTICAL, { rightLaserTarget.active }))
        place('O', Door(HORIZONTAL, { rightLaserTarget.active }))
        // Mirrors
        val leftMirror = Mirror()
        val rightMirror = Mirror(true)
        place('P', leftMirror)
        place('Q', rightMirror)
        place('R', Mirror())
        place('S', Mirror(true))
        place('T', Mirror())
        place('U', Mirror(true))
        // Laser door control
        val laserDoorSwitches = List(9, { FloorSwitch() })
        listOf('h', 'i', 'j', 'k', 'l', 'm', 'n').forEachIndexed { index, marker ->
            place(marker, laserDoorSwitches[index])
        }
        place('o', FlyingTriangle(LEFT, tileDuration = 100))
        place('p', FlyingTriangle(RIGHT, tileDuration = 100))
        // Laser doors
        listOf('a', 'b', 'c', 'd', 'e', 'f', 'g').forEachIndexed { index, marker ->
            place(marker, Door(HORIZONTAL, { laserDoorSwitches[index].pressed }))
        }
        // Faucets
        val faucets = listOf(Faucet(Direction.UP, RIGHT, 0),
                Faucet(Direction.UP, RIGHT, 0),
                Faucet(Direction.UP, LEFT, 0))
        place('V', faucets[0])
        place('W', faucets[1])
        place('X', faucets[2])
        // Triangles
        place('q', FlyingTriangle(RIGHT))
        place('r', FlyingTriangle(RIGHT))
        place('s', FlyingTriangle(RIGHT))
        place('t', FlyingTriangle(LEFT))
        place('u', FlyingTriangle(LEFT))
        place('v', FlyingTriangle(LEFT))
        // Right maze
        place('w', FlyingTriangle(RIGHT, tileDuration = 200))
        place('x', FlyingTriangle(RIGHT, tileDuration = 200))
        place('y', FlyingTriangle(LEFT, tileDuration = 200))
        place('z', FlyingTriangle(LEFT, tileDuration = 200))
        place('Y', FlyingTriangle(LEFT))
        place('Z', Cube())
        // Left maze
        val leftSwitch1 = FloorSwitch()
        val leftSwitch2 = FloorSwitch()
        place('0', leftSwitch1)
        place('1', leftSwitch2)
        place('2', Door(VERTICAL, { leftSwitch1.pressed || leftSwitch2.pressed }))
        place('3', Door(VERTICAL, { leftSwitch1.pressed && leftSwitch2.pressed }))
        place('4', FlyingTriangle(RIGHT, false, tileDuration = 200))
        place('5', FlyingTriangle(LEFT, true, tileDuration = 200))
        place('6', laserKey)
        // Outside
        place('7', Cube())
        place('9', Door(VERTICAL, { laserLock.unlocked }))
        // Tank switch
        var tankOpened = false
        val tankSwitch = FloorSwitch({
            if (!tankOpened) {
                tankOpened = true
                faucets.forEach {
                    it.volumePerSecond = 50
                }
            }
        })
        place('8', tankSwitch)
    }

}
