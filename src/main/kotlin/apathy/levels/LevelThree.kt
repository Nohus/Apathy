package apathy.levels

import apathy.Direction.LEFT
import apathy.Direction.RIGHT
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.entities.Door
import apathy.entities.FloorSwitch
import apathy.entities.FlyingTriangle

class LevelThree : LevelBuilder() {

    override val name = "3"

    override fun initialize() {
        var doorToggle = false
        place('A', FloorSwitch({ doorToggle = !doorToggle }))
        place('B', Door(HORIZONTAL, { doorToggle }))
        place('C', FlyingTriangle(LEFT, false))
        place('D', FlyingTriangle(LEFT, false))
        place('E', FlyingTriangle(RIGHT, false))
        place('F', FlyingTriangle(RIGHT, false))
        place('H', Door(HORIZONTAL, { !doorToggle }))
        place('G', FloorSwitch({ doorToggle = !doorToggle }))
    }

}
