package apathy.levels

import apathy.Direction.RIGHT
import apathy.Direction.UP
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.GREEN

class LevelSeven : LevelBuilder() {

    override val name = "7"
    override val time = 80

    override fun initialize() {
        place('A', Faucet(UP, RIGHT))
        val waterSwitch = FloorSwitch()
        place('B', waterSwitch)
        place('C', Door(VERTICAL, { waterSwitch.pressed }))
        place('D', FlyingTriangle(RIGHT))
        val keySwitch = FloorSwitch()
        place('E', keySwitch)
        place('F', Door(VERTICAL, { keySwitch.pressed }))
        place('G', Door(HORIZONTAL, { !keySwitch.pressed }))
        val lockSwitch = FloorSwitch()
        val key = Key("exit", GREEN)
        place('H', lockSwitch, key)
        place('I', Door(VERTICAL, { !lockSwitch.pressed }))
        val lock = KeyLock(key)
        place('J', lock, FlyingTriangle(RIGHT))
        place('K', Door(HORIZONTAL, { lock.unlocked }))
    }

}
