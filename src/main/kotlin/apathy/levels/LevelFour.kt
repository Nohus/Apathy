package apathy.levels

import apathy.Direction.LEFT
import apathy.Direction.RIGHT
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.entities.Cube
import apathy.entities.Door
import apathy.entities.FloorSwitch
import apathy.entities.FlyingTriangle

class LevelFour : LevelBuilder() {

    override val name = "4"

    override fun initialize() {
        val switch = FloorSwitch()
        place('B', switch)
        place('A', Door(HORIZONTAL, { switch.pressed }))
        place('C', FlyingTriangle(RIGHT, tileDuration = 75))
        place('D', FlyingTriangle(LEFT, tileDuration = 75))
        place('E', Cube())
    }

}
