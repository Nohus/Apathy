package apathy.levels

import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.entities.Door
import apathy.entities.FloorSwitch

class LevelTwo : LevelBuilder() {

    override val name = "2"

    override fun initialize() {
        val switch = FloorSwitch()
        val door = Door(HORIZONTAL, { switch.toggled })
        place('A', switch)
        place('B', door)
    }

}
