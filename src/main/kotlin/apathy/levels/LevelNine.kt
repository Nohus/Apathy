package apathy.levels

import apathy.Direction.*
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.CYAN
import com.googlecode.lanterna.TextColor.ANSI.MAGENTA

class LevelNine : LevelBuilder() {

    override val name = "9"
    override val time = 120

    override fun initialize() {
        // Bottom laser
        place('A', LaserGenerator())
        val bottomLaterSwitch = FloorSwitch()
        place('B', bottomLaterSwitch, Cube())
        place('C', Door(HORIZONTAL, { !bottomLaterSwitch.pressed }))
        // Top laser
        place('D', LaserGenerator())
        val topLaserSwitch = FloorSwitch()
        place('E', topLaserSwitch, Cube())
        place('F', Door(HORIZONTAL, { !topLaserSwitch.pressed }))
        place('G', Cube())
        // Keys
        val leftKey = Key("left", MAGENTA)
        val rightKey = Key("right", CYAN)
        place('H', leftKey)
        place('I', rightKey)
        // Mirrors
        val leftMirror = Mirror()
        val rightMirror = Mirror()
        place('J', leftMirror)
        place('K', rightMirror)
        place('L', KeyLock(leftKey, { leftMirror.toggleInverted() }))
        place('M', KeyLock(rightKey, { rightMirror.toggleInverted() }))
        // Exit
        val leftLaserTarget = LaserTarget()
        val rightLaserTarget = LaserTarget()
        place('N', leftLaserTarget)
        place('O', rightLaserTarget)
        val leftExitSwitch = FloorSwitch()
        val rightExitSwitch = FloorSwitch()
        place('k', leftExitSwitch)
        place('l', rightExitSwitch)
        place('P', Door(HORIZONTAL, { leftLaserTarget.active && rightLaserTarget.active && leftExitSwitch.pressed && rightExitSwitch.pressed }))
        // Right maze
        val rightMazeSwitch = FloorSwitch()
        place('Q', rightMazeSwitch)
        place('R', Door(HORIZONTAL, { rightMazeSwitch.pressed }))
        // Keys maze
        val keyMazeMirror = Mirror()
        place('S', keyMazeMirror)
        place('T', Mirror(true))
        place('U', Mirror())
        place('V', FloorSwitch({ keyMazeMirror.toggleInverted() }))
        place('W', FlyingTriangle(DOWN, tileDuration = 300))
        val keyMazeSwitch = FloorSwitch()
        place('X', keyMazeSwitch)
        place('Y', Door(HORIZONTAL, { keyMazeSwitch.pressed }))
        // Left passage
        val leftPassageSwitch = FloorSwitch()
        place('Z', leftPassageSwitch)
        place('a', Door(VERTICAL, { leftPassageSwitch.pressed }))
        // Top cube switch lock
        val cubeLock1 = FloorSwitch()
        val cubeLock2 = FloorSwitch()
        val cubeLock3 = FloorSwitch()
        place('b', cubeLock1)
        place('c', cubeLock2)
        place('d', cubeLock3)
        place('e', Door(VERTICAL, { cubeLock1.pressed }))
        place('f', Door(VERTICAL, { cubeLock2.pressed }))
        place('g', Door(VERTICAL, { cubeLock3.pressed }))
        val topSwitchLock = FloorSwitch()
        place('h', topSwitchLock)
        place('i', Door(VERTICAL, { topSwitchLock.toggled }))
        place('j', Door(VERTICAL, { leftPassageSwitch.pressed }))
        // Right maze triangles
        place('m', FlyingTriangle(LEFT, false, 100))
        place('n', FlyingTriangle(RIGHT, false, 100))
        place('o', FlyingTriangle(RIGHT, true, 100))
        place('p', FlyingTriangle(LEFT, true, 100))
    }

}
