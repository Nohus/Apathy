package apathy.levels

import apathy.Direction.RIGHT
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.YELLOW

class LevelEight : LevelBuilder() {

    override val name = "8"
    override val time = 80

    override fun initialize() {
        place('A', LaserGenerator())
        val primaryMirror = Mirror()
        val leftMirror = Mirror()
        val rightMirror = Mirror()
        place('B', primaryMirror)
        place('C', leftMirror)
        place('D', rightMirror)
        place('E', Mirror(true))
        place('P', Mirror(true))
        place('Q', Mirror(true))

        val key = Key("backdoor", YELLOW)
        val lock = KeyLock(key)
        place('F', key)
        place('G', lock)
        place('H', Door(HORIZONTAL, { lock.unlocked }))
        place('I', Door(VERTICAL, { lock.unlocked }))
        val laserShutdownSwitch = FloorSwitch()
        place('J', laserShutdownSwitch)
        place('K', Cube())
        place('L', Door(HORIZONTAL, { !laserShutdownSwitch.pressed } ))

        place('M', FloorSwitch({ primaryMirror.toggleInverted() }))
        place('N', FloorSwitch({ leftMirror.toggleInverted(); rightMirror.toggleInverted() }))
        place('O', FlyingTriangle(RIGHT, tileDuration = 50))
    }

}
