package apathy.levels

import apathy.Direction.*
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.GREEN

class LevelSix : LevelBuilder() {

    override val name = "6"
    override val time = 60

    override fun initialize() {
        val switch = FloorSwitch()
        val key = Key("exit", GREEN)
        place('A', switch, key)
        val lock = KeyLock(key)
        place('B', lock)
        place('C', Door(HORIZONTAL, { lock.unlocked }))
        place('D', Cube())

        place('E', Faucet(RIGHT, UP, 100))
        place('F', Faucet(RIGHT, DOWN, 100))
        place('G', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
        place('H', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
        place('I', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
        place('J', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
        place('K', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
        place('L', Door(VERTICAL, { !switch.pressed || lock.unlocked } ))
    }

}
