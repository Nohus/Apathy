package apathy.levels

import apathy.Direction.*
import apathy.LevelBuilder
import apathy.Orientation.HORIZONTAL
import apathy.Orientation.VERTICAL
import apathy.entities.*
import com.googlecode.lanterna.TextColor.ANSI.GREEN

class LevelTest : LevelBuilder() {

    override val name = "test"

    override fun initialize() {
        // First room
        val switch = FloorSwitch()
        val door = Door(VERTICAL) { switch.pressed }
        place('A', switch)
        place('C', door)
        place('B', Cube())
        place('N', Cube())
        // Laser
        place('D', LaserGenerator())
        val laserSwitch = FloorSwitch()
        place('M', laserSwitch)
        place('L', Door(VERTICAL) { laserSwitch.pressed })
        place('E', Mirror())
        place('F', Mirror(true))
        place('G', Mirror())
        val switchableMirror = Mirror(true)
        place('H', switchableMirror)
        // Fluid
        place('I', Faucet(UP, RIGHT))
        val fluidSwitch = FloorSwitch()
        place('K', fluidSwitch)
        place('J', Door(HORIZONTAL) { fluidSwitch.pressed })
        place('S', Faucet(UP, LEFT, 1))
        // Key
        val key = Key("green", GREEN)
        val keyLock = KeyLock(key)
        place('P', keyLock)
        place('Q', key)
        place('O', Door(HORIZONTAL) { keyLock.unlocked })
        // Mirror switch
        val mirrorSwitch = FloorSwitch()
        place('R', mirrorSwitch)
        place('T', LaserTarget())
        addLevelTick {
            switchableMirror.setInverted(!mirrorSwitch.pressed)
        }
        // Flying triangle
        place('U', FlyingTriangle(RIGHT, true))
        // Exit
        place('V', Exit())
    }

}
