package apathy.levels

import apathy.LevelBuilder

class LevelOne : LevelBuilder() {

    override val name = "1"

    override fun initialize() {}

}
