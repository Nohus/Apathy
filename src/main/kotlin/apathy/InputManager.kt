package apathy

import apathy.Direction.*
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType

/**
 * Manages keyboard input to the game and handles actions to perform
 * for every relevant key press
 */
class InputManager {

    /**
     * Process all key presses in the buffer
     */
    fun processInput() {
        while (true) {
            val key = terminal.pollInput()
            if (key != null) {
                processKey(key)
            } else {
                break
            }
        }
    }

    private fun processKey(key: KeyStroke) {
        when (key.keyType) {
            KeyType.Character -> {
                when (key.character) {
                    'w' -> game.move(UP)
                    'a' -> game.move(LEFT)
                    's' -> game.move(DOWN)
                    'd' -> game.move(RIGHT)
                    ' ' -> game.pickup()
                    'p' -> game.togglePause()
                    'r' -> game.restartLevel()
                    'g' -> if (key.isCtrlDown) { game.winGame() } // Yes, this is a cheat
                }
            }
            KeyType.ArrowUp -> game.inventory.moveSelectionUp()
            KeyType.ArrowDown -> game.inventory.moveSelectionDown()
            KeyType.Enter -> { if (game.isPlayable) game.drop() else game.continuePlaying() }
            else -> {}
        }
    }

}
