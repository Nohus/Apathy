package apathy
import apathy.entities.Entity
import apathy.entities.LaserBeam
import com.googlecode.lanterna.TextColor.ANSI.GREEN
import com.googlecode.lanterna.TextColor.ANSI.RED

/**
 * The player character
 */
class Player : Entity() {

    override val name = "player"
    override val sprite
    get() = if (alive) Sprite('☻', GREEN) else Sprite('☹', RED)
    override val isBlocking = true

    private var alive = true

    override fun onHitByLaser(laser: LaserBeam) {
        kill("Vaporized by a laser beam")
    }

    fun kill(reason: String) {
        if (alive && !game.levelWon) {
            alive = false
            game.loseGame(reason)
        }
    }

}
