package apathy.entities

import apathy.Direction
import apathy.Direction.*
import apathy.Sprite
import apathy.game
import apathy.to

class Faucet(from: Direction, private val output: Direction, var volumePerSecond: Int = 25) : Entity() {

    override val name = "faucet"
    override val sprite = when {
        from == UP && output == LEFT -> Sprite('╜')
        from == UP && output == RIGHT -> Sprite('╙')
        from == DOWN && output == LEFT -> Sprite('╖')
        from == DOWN && output == RIGHT -> Sprite('╓')
        from == LEFT && output == UP -> Sprite('╛')
        from == LEFT && output == DOWN -> Sprite('╕')
        from == RIGHT && output == UP -> Sprite('╘')
        from == RIGHT && output == DOWN -> Sprite('╒')
        else -> Sprite('?')
    }
    override val isBlocking = true

    private var volumeAccumulator = 0.0

    override fun onTick(deltaTime: Long) {
        val outflow = volumePerSecond * (deltaTime / 1000.0)
        volumeAccumulator += outflow
        while (volumeAccumulator >= 1.0) {
            volumeAccumulator -= 1.0
            val pos = pos!!.to(output)
            if (!game.level.isBlockingExcept(pos, { it.name == "water" })) {
                // There is space to output fluid
                val fluid = game.level.getTile(pos)?.getEntities()?.lastOrNull { it.name == "water" } as Fluid?
                if (fluid != null) {
                    fluid.addVolume(1)
                } else {
                    game.level.place(pos, Fluid("water", 1))
                }
            }
        }
    }

}
