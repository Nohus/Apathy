package apathy.entities

import apathy.*
import apathy.Direction.*
import com.googlecode.lanterna.TextColor.ANSI.MAGENTA

class FlyingTriangle(
        private var direction: Direction,
        private var clockwise: Boolean = true,
        private val tileDuration: Long = 300L
) : Entity() {

    override val name = "flying triangle"
    private val color = MAGENTA
    private val directionSprites = mapOf(
            LEFT to Sprite(200, Drawable('◂', color), Drawable('◀', color)),
            RIGHT to Sprite(200, Drawable('▸', color), Drawable('▶', color)),
            UP to Sprite(200, Drawable('▴', color), Drawable('▲', color)),
            DOWN to Sprite(200, Drawable('▾', color), Drawable('▼', color))
    )
    override val sprites = directionSprites.values.toList()
    override val sprite: Sprite
        get() = directionSprites[direction]!!
    override val isBlocking = false

    private var tileTime = 0L

    override fun onTick(deltaTime: Long) {
        tileTime += deltaTime
        if (tileTime >= tileDuration) {
            tileTime -= tileDuration
            move()
        }
    }

    override fun onPlayer() {
        game.level.player.kill("Pierced by a $name")
    }

    override fun onHitByLaser(laser: LaserBeam) {
        despawn()
    }

    private fun move() {
        val targetTile = pos!!.to(direction)
        val canMove = !game.level.isBlockingExcept(targetTile, { it is Player })
        if (canMove) {
            game.level.place(targetTile, this)
        } else {
            direction = direction.rotate(clockwise)
        }
    }

}
