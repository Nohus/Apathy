package apathy.entities

import apathy.Sprite

class Wall(
        override val sprite: Sprite
) : Entity() {

    override val name = "wall"
    override val isBlocking = true

}
