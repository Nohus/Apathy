package apathy.entities

import apathy.Drawable
import apathy.Sprite

class FloorSwitch(private val onPress: () -> Unit = {}) : Entity() {

    override val name = "switch"
    override val sprite = Sprite(250,
            Drawable('◰'),
            Drawable('◳'),
            Drawable('◲'),
            Drawable('◱'))
    override val isBlocking = false

    var pressed = false
    private set
    var toggled = false
    private set

    override fun onTick(deltaTime: Long) {
        val tile = getTile()
        val layer = tile.getEntities().indexOf(this)
        val nowPressed = layer < tile.getEntities().filter { it !is LaserBeam }.lastIndex // Something is on the switch
        if (nowPressed && !pressed) {
            toggled = !toggled
            onPress()
        }
        pressed = nowPressed
    }

}
