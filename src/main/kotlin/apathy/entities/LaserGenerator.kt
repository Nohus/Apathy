package apathy.entities

import apathy.*
import apathy.Direction.*
import com.googlecode.lanterna.TextColor

class LaserGenerator : Entity() {

    override val name = "laser generator"
    override val sprite = Sprite(250,
            Drawable('⏇', TextColor.ANSI.RED),
            Drawable('⏈', TextColor.ANSI.RED),
            Drawable('⏉', TextColor.ANSI.YELLOW),
            Drawable('⏊', TextColor.ANSI.YELLOW))
            override val isBlocking = true

    override fun onTick(deltaTime: Long) {
        listOf(UP, DOWN, LEFT, RIGHT).forEach {
            if (!game.level.isBlocking(pos!!.to(it))) {
                if (getTile(it)!!.getEntities().none { it is LaserBeam }) {
                    game.level.place(pos!!.to(it), LaserBeam(it.orientation(), it, this))
                }
            }
        }
    }

}
