package apathy.entities

import apathy.Orientation
import apathy.Orientation.VERTICAL
import apathy.Sprite
import apathy.game
import com.googlecode.lanterna.TextColor.ANSI.YELLOW

class Door(private val orientation: Orientation, private val isOpen: () -> Boolean) : Entity() {

    override val name: String
    get() = if (isOpen()) "door frame" else "door"
    override val sprite: Sprite
        get() {
            return if (isOpen()) {
                if (orientation == VERTICAL) {
                    Sprite('▔', YELLOW)
                } else {
                    Sprite('▏', YELLOW)
                }
            } else {
                if (orientation == VERTICAL) {
                    Sprite('│', YELLOW)
                } else {
                    Sprite('─', YELLOW)
                }
            }
        }
    override val isBlocking: Boolean
        get() = !isOpen()

    override fun onTick(deltaTime: Long) {
        if (!isOpen()) {
            resurface()
        } else {
            desurface()
        }
    }

    override fun onPlayer() {
        if (!isOpen()) {
            game.level.player.kill("Crushed by a closing door")
        }
    }

}
