package apathy.entities

import apathy.*
import apathy.Orientation.HORIZONTAL
import com.googlecode.lanterna.TextColor.ANSI.RED

class LaserBeam(
        private val orientation: Orientation,
        val direction: Direction,
        private val source: Entity) : Entity() {

    override val name = "laser beam"
    private val normalSprite = when (orientation) {
        HORIZONTAL -> Sprite(100,
                Drawable('─', RED),
                Drawable('─', RED),
                Drawable('─', RED),
                Drawable('━', RED))
        else -> Sprite(100,
                Drawable('│', RED),
                Drawable('│', RED),
                Drawable('│', RED),
                Drawable('┃', RED))
    }
    private val hittingSprite = Sprite(100,
            Drawable('✶', RED),
            Drawable('✷', RED),
            Drawable('✸', RED),
            Drawable('✹', RED),
            Drawable('✺', RED))
    override val sprite
        get() = if (laserEnd) hittingSprite else normalSprite
    override val sprites = listOf(normalSprite, hittingSprite)
    override val isBlocking = false

    init {
        if (source is LaserBeam) {
            normalSprite.copyAnimation(source.normalSprite, -25)
        }
    }

    var laserEnd = false

    override fun onTick(deltaTime: Long) {
        // Despawn if source is missing or tile is blocked
        if (!source.isOnMap() || game.level.isBlocking(pos!!)) {
            despawn()
        } else {
            // Continue with the laser beam
            val nextPos = pos!!.to(direction)
            if (!game.level.isBlocking(nextPos)) { // Laser should continue
                if (!game.level.getTile(nextPos)!!.getEntities().any { it is LaserBeam }) { // And there is no laser further
                    game.level.place(pos!!.to(direction), LaserBeam(orientation, direction, this))
                }
                laserEnd = false
            } else { // Laser hit something
                val target = game.level.getTile(nextPos)?.getEntities()?.last { it.isBlocking }
                val acceptsLaser = target?.acceptsLaser ?: false
                laserEnd = !acceptsLaser
                target?.onHitByLaser(this)
            }
            getTile().getEntities().forEach { it.onHitByLaser(this) }
        }
    }

}
