package apathy.entities

import apathy.Direction.*
import apathy.Player
import apathy.Sprite
import apathy.game
import apathy.to
import com.googlecode.lanterna.TextColor.ANSI.BLUE
import com.googlecode.lanterna.TextColor.ANSI.WHITE

class Fluid(override val name: String, private var volume: Int = 100) : Entity() {

    override val sprite: Sprite
        get() = when (volume) {
            in 75..100 -> Sprite('█', color)
            in 50..74 -> Sprite('▓', color)
            in 25..49 -> Sprite('▒', color)
            in 1..24 -> Sprite('░', color)
            else -> Sprite('?')
        }
    override val isBlocking = true

    private val color = when (name) {
        "steam" -> WHITE
        else -> BLUE
    }
    // More viscosity = less likely to flow
    private val viscosity = when (name) {
        "steam" -> 100
        else -> 250
    }
    // Controls the size of the smallest volume that won't flow further
    private val surfaceTension = when (name) {
        "steam" -> 1
        else -> 10
    }
    // Controls the volume under which the fluid will disappear
    private val evaporationVolume = when (name) {
        "steam" -> 2
        else -> 0
    }
    var timeInPlace = 0L

    override fun onTick(deltaTime: Long) {
        timeInPlace += deltaTime
        if (timeInPlace >= viscosity) {
            timeInPlace -= viscosity
            // Reached time to try flowing
            listOf(UP, DOWN, LEFT, RIGHT).shuffled().forEach { flowDirection ->
                if (volume >= surfaceTension) {
                    if (pos == null) {
                        return // Fluid may evaporate neighbors mid-tick
                    }
                    val pos = pos!!.to(flowDirection)
                    if (!game.level.isBlockingExcept(pos, { it.name == name || it is Player })) {
                        val adjacentFluid = game.level.getTile(pos)?.getEntities()?.lastOrNull { it.name == name } as Fluid?
                        if (adjacentFluid == null) {
                            val flowVolume = volume / 2 + 1
                            volume -= flowVolume
                            game.level.place(pos, Fluid(name, flowVolume))
                        } else if (game.level.getTile(pos)?.getEntities()?.any { it is LaserBeam } == true) {
                            // Tried to flow into a laser beam
                            boil()
                        } else {
                            if (adjacentFluid.volume < volume) { // There is less fluid than here, we can flow into it
                                val flowVolume = (volume - adjacentFluid.volume) / 2 + 1
                                volume -= flowVolume
                                adjacentFluid.volume += flowVolume
                            }
                        }
                    }
                }
            }
        }
        if (volume <= evaporationVolume) {
            despawn()
        }
    }

    override fun onHitByLaser(laser: LaserBeam) {
        if (name == "water") {
            val adjacentTiles = listOf(LEFT, UP, RIGHT, DOWN).map { getTile(it) }
            boil()
            adjacentTiles.forEach {
                it?.getEntities()?.lastOrNull { it.name == "water" }?.let { (it as Fluid).boil() }
            }
        }
    }

    private fun boil() {
        game.level.place(pos!!, Fluid("steam", volume))
        despawn()
    }

    override fun onPlayer() {
        game.level.player.kill("Drowned in $name")
    }

    fun addVolume(volume: Int) {
        this.volume += volume
        if (this.volume > 100) {
            this.volume = 100
        }
    }

}
