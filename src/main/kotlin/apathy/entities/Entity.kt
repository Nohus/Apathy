package apathy.entities

import apathy.*

/**
 * An in-game object
 */
abstract class Entity {

    var pos: Pos? = null

    abstract val name: String
    abstract val sprite: Sprite
    abstract val isBlocking: Boolean
    open val pickupable: Boolean = false
    // In case the entity has multiple sprites (that might switch), they should be put here to be ticked even when off
    open val sprites: List<Sprite> = emptyList()
    // Will cause lasers to not appear to hit this entity
    open val acceptsLaser = false

    fun setPosition(pos: Pos) {
        this.pos = pos
    }

    fun resetPosition() {
        pos = null
    }

    fun isOnMap(): Boolean {
        return pos != null
    }

    /**
     * Get the drawable representation of this Entity at this moment
     */
    fun getDrawable(): Drawable {
        return sprite.getDrawable()
    }

    /**
     * Returns the tile the entity is currently on
     */
    fun getTile(): Tile {
        return game.level.getTile(pos!!)!!
    }

    /**
     * Returns an adjacent tile
     */
    fun getTile(direction: Direction): Tile? {
        return game.level.getTile(pos!!.to(direction))
    }

    fun despawn() {
        game.level.remove(this)
    }

    /**
     * Move the entity to the top of the tile stack
     */
    fun resurface() {
        game.level.place(pos!!, this)
    }

    /**
     * Move the entity to the bottom of the tile stack
     */
    fun desurface() {
        val entities = getTile().getEntities().filter { it != this }
        resurface()
        entities.forEach { it.resurface() }
    }

    /**
     * Ticks the sprites of this Entity and calls
     * subclass-specific tick actions
     */
    fun tick(deltaTime: Long) {
        if (sprites.isEmpty()) {
            sprite.tick(deltaTime)
        } else {
            sprites.forEach {
                it.tick(deltaTime)
            }
        }
        onTick(deltaTime)
    }

    open fun onTick(deltaTime: Long) {}
    open fun onPickup() {}
    open fun onHitByLaser(laser: LaserBeam) {}
    open fun onPlayer() {}

}
