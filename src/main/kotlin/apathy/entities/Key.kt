package apathy.entities

import apathy.Sprite
import com.googlecode.lanterna.TextColor

class Key(val keyName: String, val color: TextColor.ANSI) : Entity() {

    override val name = "$keyName key"
    override val sprite = Sprite('F', color)
    override val isBlocking = false
    override val pickupable = true

}
