package apathy.entities

import apathy.*
import apathy.Direction.*
import com.googlecode.lanterna.TextColor.ANSI.BLUE

class Mirror(private var inverted: Boolean = false) : Entity() {

    override val name = "mirror"
    override val sprite
    get() = if (inverted) Sprite('╱', BLUE) else Sprite('╲', BLUE)
    override val isBlocking = true
    override val acceptsLaser = true

    fun setInverted(inverted: Boolean) {
        if (this.inverted == inverted) {
            return
        }
        this.inverted = inverted
        // Remove output lasers, they are going to be invalid after the inversion
        listOf(LEFT, UP, RIGHT, DOWN).forEach { direction ->
            game.level.getTile(pos!!.to(direction))?.getEntities()?.lastOrNull { it is LaserBeam && it.direction == direction }?.despawn()
        }
    }

    fun toggleInverted() {
        setInverted(!inverted)
    }

    override fun onTick(deltaTime: Long) {
        val mirrorMappings = if (!inverted) {
            listOf(LEFT to DOWN, DOWN to LEFT, RIGHT to UP, UP to RIGHT)
        } else {
            listOf(LEFT to UP, UP to LEFT, RIGHT to DOWN, DOWN to RIGHT)
        }
        mirrorMappings.forEach { mapping ->
            if (game.level.anyOnTile(pos!!.to(mapping.first), { it is LaserBeam && it.direction == mapping.first.reverse() })) {
                val sourceLaser = game.level.getTile(pos!!.to(mapping.first))!!.getEntities().first { it is LaserBeam } as LaserBeam
                if (game.level.noneOnTile(pos!!.to(mapping.second), { it is LaserBeam })) {
                    game.level.place(pos!!.to(mapping.second), LaserBeam(mapping.second.orientation(), mapping.second, sourceLaser))
                }
            }
        }
    }

}
