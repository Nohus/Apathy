package apathy.entities

import apathy.Drawable
import apathy.Sprite
import com.googlecode.lanterna.TextColor.ANSI.RED

class LaserTarget : Entity() {

    override val name = "laser target"
    private val activeSprite = Sprite(100,
            Drawable('◓', RED),
            Drawable('◑', RED),
            Drawable('◒', RED),
            Drawable('◐', RED))
    private val normalSprite = Sprite(250,
            Drawable('◴'),
            Drawable('◷'),
            Drawable('◶'),
            Drawable('◵'))
    override val sprites = listOf(activeSprite, normalSprite)
    override val sprite
    get() = if (active) activeSprite else normalSprite
    override val isBlocking = true
    override val acceptsLaser = true

    var activeLaser: LaserBeam? = null
    var active = false

    override fun onHitByLaser(laser: LaserBeam) {
        active = true
        activeLaser = laser
    }

    override fun onTick(deltaTime: Long) {
        if (active) {
            if (activeLaser?.isOnMap() == false) {
                active = false
                activeLaser = null
            }
        }
    }

}
