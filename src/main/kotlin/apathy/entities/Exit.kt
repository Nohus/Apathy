package apathy.entities

import apathy.*
import com.googlecode.lanterna.TextColor.ANSI.*

class Exit(private val plane: Boolean = false) : Entity() {

    override val name = "exit"
    override val sprite = if (plane) {
        Sprite('✈', GREEN)
    } else {
        Sprite(200,
                Drawable('▤', CYAN),
                Drawable('▧', CYAN),
                Drawable('▥', CYAN),
                Drawable('▨', CYAN))
    }
    override val isBlocking = false
    private var spawnedRunway = false

    override fun onTick(deltaTime: Long) {
        if (plane && !spawnedRunway) {
            spawnedRunway = true
            spawnRunwaySide(-1)
            spawnRunwaySide(1)
        }
    }

    private fun spawnRunwaySide(yOffset: Int) {
        for (i in 0..11) {
            if ((i - 1) % 3 == 0) {
                game.level.place(pos!! + Pos(i, yOffset), Wall(Sprite(300, i * -20L,
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌'),
                        Drawable('╌', RED))))
            } else {
                game.level.place(pos!! + Pos(i, yOffset), Wall(Sprite('┈')))
            }
        }
        listOf(3, 8).forEach { xOffset ->
            game.level.place(pos!! + Pos(xOffset, yOffset * 2), Wall(Sprite(500, xOffset * -100L,
                    Drawable('⊷', BLUE),
                    Drawable('⊷', BLUE),
                    Drawable('⊷', BLUE),
                    Drawable('⊶', GREEN))))
        }
    }

    override fun onPlayer() {
        if (game.isPlayable) {
            game.winGame()
        }
    }

}
