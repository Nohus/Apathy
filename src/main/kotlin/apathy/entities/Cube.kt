package apathy.entities

import apathy.Sprite
import com.googlecode.lanterna.TextColor.ANSI.GREEN

class Cube : Entity() {

    override val name: String
    get() = if (activated) "shining cube" else "cube"
    override val sprite: Sprite
        get() {
            return if (activated) {
                Sprite('▣', GREEN)
            } else {
                Sprite('▪')
            }
        }
    override val isBlocking = false
    override val pickupable = true

    var activated = false

    override fun onTick(deltaTime: Long) {
        activated = getTile().getEntities().any { it is FloorSwitch }
    }

    override fun onPickup() {
        activated = false
    }

}
