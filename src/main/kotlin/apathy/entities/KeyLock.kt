package apathy.entities

import apathy.Sprite

class KeyLock(
        private val key: Key,
        private val onUnlock: () -> Unit = {}
) : Entity() {

    override val name
    get() = if (unlocked) {
        "unlocked ${key.keyName} lock"
    } else {
        "${key.keyName} lock"
    }
    override val sprite
    get() = if (unlocked) {
        Sprite('⊛', key.color)
    } else {
        Sprite('⊝', key.color)
    }
    override val isBlocking = false

    var unlocked = false
    private set

    override fun onTick(deltaTime: Long) {
        if (!unlocked) {
            val insertedKey = getTile().getEntities().firstOrNull { it == key }
            if (insertedKey != null) {
                insertedKey.despawn()
                unlocked = true
                onUnlock()
            }
        }
    }

}
