package apathy

import apathy.entities.Entity
import java.lang.IndexOutOfBoundsException

/**
 * A single level in the game
 */
class Level(var timeLeft: Long) {

    companion object {
        const val WIDTH = 58
        const val HEIGHT = 18
    }

    private val tiles: Array<Array<Tile>>
    var levelTick: (Long) -> Unit = {}
    val player = Player()

    init {
        tiles = Array(WIDTH, { Array(HEIGHT, { Tile() }) })
    }

    /**
     * Place an entity on the level
     */
    fun place(pos: Pos, entity: Entity) {
        if (entity.isOnMap()) {
            remove(entity)
        }
        tiles[pos.x][pos.y].place(entity)
        entity.setPosition(pos)
    }

    /**
     * Remove an entity from the level
     */
    fun remove(entity: Entity) {
        if (entity.isOnMap()) {
            tiles[entity.pos!!.x][entity.pos!!.y].remove(entity)
            entity.resetPosition()
        } else {
            println("Trying to remove an entity that doesn't believe to be on the level")
        }
    }

    /**
     * Returns the Drawable that should be used to represent
     * a position in the level at this moment
     */
    fun getDrawable(pos: Pos): Drawable {
        return tiles[pos.x][pos.y].getDrawable()
    }

    /**
     * Ticks all entities in the level, fires tick-specific events,
     * fires the global level ticks and advances elapsed time
     */
    fun tick(deltaTime: Long) {
        getTile(player.pos)!!.getEntities().reversed().forEach { it.onPlayer() }
        for (x in 0 until WIDTH) {
            for (y in 0 until HEIGHT) {
                tiles[x][y].getEntities().forEach {
                    it.tick(deltaTime)
                }
            }
        }
        levelTick(deltaTime)
        if (game.isPlayable) {
            timeLeft -= deltaTime
            if (timeLeft <= 0) {
                player.kill("Ran out of time")
            }
        }
    }

    private fun isInsideLevel(pos: Pos): Boolean {
        if (pos.x < 0 || pos.x >= WIDTH) return false
        if (pos.y < 0 || pos.y >= HEIGHT) return false
        return true
    }

    /**
     * Checks if any entity is blocking this position
     */
    fun isBlocking(pos: Pos): Boolean {
        if (!isInsideLevel(pos)) {
            return true
        }
        return tiles[pos.x][pos.y].getEntities().any { it.isBlocking }
    }

    /**
     * Checking if any entity is blocking this position, except entities covered by the predicate
     */
    fun isBlockingExcept(pos: Pos, predicate: (Entity) -> Boolean): Boolean {
        if (!isInsideLevel(pos)) {
            return true
        }
        return tiles[pos.x][pos.y].getEntities().any { it.isBlocking && !predicate(it) }
    }

    fun getTile(pos: Pos?): Tile? {
        if (pos == null) {
            return null
        }
        return try {
            tiles[pos.x][pos.y]
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    /**
     * Checks if any entity on a given tile satisfies the given predicate
     */
    fun anyOnTile(pos: Pos, entityPredicate: (Entity) -> Boolean): Boolean {
        return getTile(pos)?.getEntities()?.any(entityPredicate) ?: false
    }

    /**
     * Checks if none of the entities on a given tile satisfy the given predicate
     */
    fun noneOnTile(pos: Pos, entityPredicate: (Entity) -> Boolean): Boolean {
        return getTile(pos)?.getEntities()?.none(entityPredicate) ?: false
    }

}
