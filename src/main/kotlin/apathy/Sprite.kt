package apathy

import com.googlecode.lanterna.TextColor.ANSI

/**
 * A collection of optionally animated Drawables to be used
 * as a graphical representation of Entities
 */
class Sprite {

    constructor(character: Char, color: ANSI = ANSI.DEFAULT) {
        animationFrames = listOf(Drawable(character, color))
    }

    constructor(frameDuration: Long, vararg frames: Drawable) {
        this.frameDuration = frameDuration
        animationFrames = frames.toList()
    }

    constructor(frameDuration: Long, animationOffset: Long, vararg frames: Drawable) {
        this.frameDuration = frameDuration
        animationFrames = frames.toList()
        frameTimeLeft -= animationOffset
    }

    private val animationFrames: List<Drawable>
    private var frameDuration = 0L
    private var currentFrame = 0
    private var frameTimeLeft = 0L

    /**
     * Advance the sprite animation in time
     */
    fun tick(deltaTime: Long) {
        if (frameDuration > 0) {
            frameTimeLeft -= deltaTime
            while (frameTimeLeft < 0) {
                currentFrame++
                if (currentFrame > animationFrames.lastIndex) {
                    currentFrame = 0
                }
                frameTimeLeft += frameDuration
            }
        }
    }

    /**
     * Get the Drawable at this point in time
     */
    fun getDrawable(): Drawable {
        return animationFrames[currentFrame]
    }

    /**
     * Copy the animation state of another Sprite into this Sprite, offsetting the animation
     * timing by the specified time. This is useful for creating "animation waves" of many Entities.
     */
    fun copyAnimation(source: Sprite, animationOffset: Long) {
        frameDuration = source.frameDuration
        currentFrame = source.currentFrame
        frameTimeLeft = source.frameTimeLeft
        frameTimeLeft -= animationOffset
    }

}
