package apathy

import com.googlecode.lanterna.TextColor.ANSI

/**
 * A colored character that can be drawn on the screen
 */
data class Drawable(
            val character: Char,
            val color: ANSI = ANSI.DEFAULT
)
